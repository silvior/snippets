// Referance:
// https://developer.mozilla.org/en-US/docs/Web/API/Window.scrollY
// http://stackoverflow.com/questions/5454592/get-distance-of-window-from-the-top-of-document-on-page-load-javascript-only

// See debounce.js or throttle.js

var topDist, timer;

window.addEventListener('scroll', function() {
  clearTimeout(timer);

  timer = setTimeout(function(){
    topDist = window.pageYOffset;
    console.log(topDist);
  },500);
}, false);
