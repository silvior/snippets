// 'abcdefghijklmnopqrstuvwxyz'.split('');

m3()
function m3() {
  const itemsBase = 'abcdefghijklmnopqrstuvwxyz'.split('');

  const items = [...itemsBase]
  
  const rand = (a)=> Math.random() * a | 0

  const tpls = []
  tpls[0] = (item)=> `|1s${item[0]}|`
  tpls[1] = (item)=> `|2h${item[0]}${item[1]}|`
  tpls[2] = (item)=> `|2v${item[0]}${item[1]}|`
  tpls[3] = (item)=> `|3h${item[0]}${item[1]}${item[2]}|`
  tpls[4] = (item)=> `|3v${item[0]}${item[1]}${item[2]}|`
  
  let tplPicked = []
  function randTpl() {
    if (tplPicked.length >= 3) tplPicked = []
    const r = rand(4)
    if (tplPicked.some((i)=> i === r)) return randTpl()
    tplPicked.push(r)
    return r
  }
  
  function getTpl(items) {
    return items.length === 1 ? 0 :
            items.length === 2 ? rand(1)?1:2 :
            items.length === 3 ? rand(1)?3:4 : randTpl()
  }

  const layout = (item, l, arr)=> {
    let r = ''
    if (l===0) r+='|'
    r += item
    if (l===arr.length -1 ) r+='|'
    return r
  }
  
  function layouts(items, str) {
    if (!items.length) return str
    const tplNum = getTpl(items)
    const sliceNum = tplNum === 0 ? 1 :
                    tplNum === 1 || tplNum === 2 ? 2 : 3
    
    let sliced = items.splice(0,sliceNum)
    str += tpls[tplNum](sliced)
    
    return layouts(items, str)
  }
  
  console.log(layouts(items, ''))
  console.log(itemsBase)
}

m2()
function m2() {  
  const itemsBase = 'abcdefghijklm'.split('');

  const items = [...itemsBase]

  const matrix = [
    [1,0,0],
    [1,1,0],
    [1,1,1],
    [1,2,1],
    [1,2,2],
    [1,3,2],
    [1,3,3],
  ]

  const layout = (item, l, arr)=> {
    let r = ''
    if (l===0) r+='|'
    r += item
    if (l===arr.length -1 ) r+='|'
    return r
  }

  function layouts(items, str) {
    if (!items.length) return str

    let slicedItems = items.splice(0,matrix.length)
    let mx = matrix[slicedItems.length - 1]

    for (let i=0; i < mx.length; i++) {
      let sliced = slicedItems.splice(0,mx[i])
      str += sliced.map(layout).join('')
    }

    return layouts(items, str)
  }

  console.log(layouts(items, ''))
  console.log(itemsBase)
}

m1()
function m1() {
  
  const itemsBase = 'abcdefghijklmnopqrstuvwxyz'.split('');

  const items = [...itemsBase]

  const matrix = [
    [1,0,3],
    [1,2,1],
  ]

  const layout = (item, l, arr)=> {
    let r = ''
    if (l===0) r+='|'
    r += item
    if (l===arr.length -1 ) r+='|'
    return r
  }

  function layouts(items, str) {
    if (!items.length) return str

    for (let i=0; i<matrix.length; i++) {
      for (let k=0; k < matrix[i].length; k++) {
        let sliced = items.splice(0,matrix[i][k])
        str += sliced.map(layout).join('')
      }
    }

    return layouts(items, str)
  }

  console.log(layouts(items, ''))
  console.log(itemsBase)
}