function stringToObject(str) {
	// string from attr attr="{open: false, str:'This is string', arr: [1,2,'3']}"
	const jsonStr = str.replace(/'/g,'"').replace(/(\w+:)|(\w+ :)/g, function(matchedStr) {
		return '"' + matchedStr.substring(0, matchedStr.length - 1) + '":';
	})
	return JSON.parse(jsonStr)
}

const str = "{open: false, str:'This is string', arr: [1,2,'3'], subObj:{is:'ok'}}"
const obj = stringToObject(str)

console.log(obj)

console.log(obj.open === false)
console.log(obj.str === 'This is string')
console.log(Array.isArray(obj.arr) === true)
console.log(obj.arr[0] === 1)
console.log(obj.arr[2] === '3')
console.log(obj.subObj.is === 'ok')

