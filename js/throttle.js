// Reference:
// http://loopinfinito.com.br/2013/09/24/throttle-e-debounce-patterns-em-javascript/
// https://github.com/caiogondim/js-patterns-sublime-snippets#throttle
// http://benalman.com/projects/jquery-throttle-debounce-plugin/

var throttle = (function () {
	'use strict';

	var timeWindow = 500; // time in ms
	var lastExecution = new Date((new Date()).getTime() - timeWindow);

	var throttle = function (args) {
		 // your code goes here
	};

	return function () {
		if ((lastExecution.getTime() + timeWindow) <= (new Date()).getTime()) {
			lastExecution = new Date();
			return throttle.apply(this, arguments);
		}
	};
}());
