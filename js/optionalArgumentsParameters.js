// Reference:
// http://stackoverflow.com/questions/148901/is-there-a-better-way-to-do-optional-function-parameters-in-javascript

var optionalArgumentsParameters = function  ( arg ) {
	'use strict';

	arg = {
		name:    arg.name || (typeof arg.name === undefined ) ? arg.name : false,
		numMax:  arg.numMax || (typeof arg.numMax === undefined ) ? arg.numMax : 10,
		numMin:  arg.numMin || (typeof arg.numMin === undefined ) ? arg.numMin : 1,
		bolean:  arg.bolean || (typeof arg.bolean === undefined ) ? arg.bolean : false
	};

	if (!arg.name) {
		console.log('name: is required!');
		return;
	}

	var number = function () {
		return arg.numMax - arg.numMin;
	};

	var init = function () {
		console.log( 'Hello ' + arg.name +', your number is ' + number() + '. This is ' + arg.bolean + '!!!');
	};

	return init();
};

optionalArgumentsParameters({ name: 'John Doe', bolean: true, numMax: '8', numMin: 3});
