
function mapRangeNumbers(value, xMin, xMax, yMin, yMax) {
    return (value - xMin) * (yMax - yMin) / (xMax - xMin) + yMin
}

const r1 = mapRangeNumbers(1, 1, 100, 1, 10)
console.log(r1 === 1, r1)

const r2 = mapRangeNumbers(100, 1, 100, 1, 10)
console.log(r2 === 10, r2)

const r3 = mapRangeNumbers(50, 1, 100, 1, 10)
console.log(Math.floor(r3) === 5, r3)

const r4 = mapRangeNumbers(100, 1, 100, 200, 1)
console.log(r4 === 1, r4)

const r5 = mapRangeNumbers(50, 1, 100, 1, 30)
console.log(Math.floor(r5) === 15, r5)

const r6 = mapRangeNumbers(100, 1, 200, 30, 1)
console.log(Math.floor(r6) === 15, r6)
