// Reference:
// http://loopinfinito.com.br/2013/09/24/throttle-e-debounce-patterns-em-javascript/
// https://github.com/caiogondim/js-patterns-sublime-snippets#debounce
// http://benalman.com/projects/jquery-throttle-debounce-plugin/

var debounce1 = (function () {
	'use strict';

	var timeWindow = 500; // time in ms
	var timeout;

	var debounce1 = function (args) {
		 // your code goes here
	};

	return function() {
		var context = this;
		var args = arguments;
		clearTimeout(timeout);

		timeout = setTimeout(function(){
			debounce1.apply(context, args);
		}, timeWindow);
	};
}());

// Reference:
// http://modernjavascript.blogspot.pt/2013/08/building-better-debounce.html
var debounce2 = function(func, wait) {
	// we need to save these in the closure
	var timeout, args, context, timestamp;

	return function() {

		// save details of latest call
		context = this;
		args = [].slice.call(arguments, 0);
		timestamp = new Date();

		// this is where the magic happens
		var later = function() {

			// how long ago was the last call
			var last = (new Date()) - timestamp;

			// if the latest call was less that the wait period ago
			// then we reset the timeout to wait for the difference
			if (last < wait) {
				timeout = setTimeout(later, wait - last);

			   // or if not we can null out the timer and run the latest
			} else {
				timeout = null;
				func.apply(context, args);
			}
		};

		// we only need to set the timer now if one isn't already running
		if (!timeout) {
			timeout = setTimeout(later, wait);
		}
	};
};
