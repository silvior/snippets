// https://javascript.info/js-animation
function animTotalTime({timing, draw, duration}) {

  let start = performance.now();
  

  let raf = requestAnimationFrame(function animate(time) {
    // timeFraction goes from 0 to 1
    let timeFraction = (time - start) / duration;
    if (timeFraction > 1) timeFraction = 1;

    // calculate the current animation state
    let progress = timing(timeFraction);

    draw(progress); // draw it

    if (timeFraction < 1) {
      raf = requestAnimationFrame(animate);
    } else {
      // new line
      cancelAnimationFrame(raf)
    }
  });
}

// Simple Easing Functions in Javascrip
// https://gist.github.com/gre/1650294
const easingFunctions = {
  // no easing, no acceleration
  linear: t => t,
  // accelerating from zero velocity
  easeInQuad: t => t*t,
  // decelerating to zero velocity
  easeOutQuad: t => t*(2-t),
  // acceleration until halfway, then deceleration
  easeInOutQuad: t => t<.5 ? 2*t*t : -1+(4-2*t)*t,
  // accelerating from zero velocity 
  easeInCubic: t => t*t*t,
  // decelerating to zero velocity 
  easeOutCubic: t => (--t)*t*t+1,
  // acceleration until halfway, then deceleration 
  easeInOutCubic: t => t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1,
  // accelerating from zero velocity 
  easeInQuart: t => t*t*t*t,
  // decelerating to zero velocity 
  easeOutQuart: t => 1-(--t)*t*t*t,
  // acceleration until halfway, then deceleration
  easeInOutQuart: t => t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t,
  // accelerating from zero velocity
  easeInQuint: t => t*t*t*t*t,
  // decelerating to zero velocity
  easeOutQuint: t => 1+(--t)*t*t*t*t,
  // acceleration until halfway, then deceleration 
  easeInOutQuint: t => t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t
}

/* 

// tests with dom 100x100px .box

const rec = document.querySelectorAll('.box')

function create(tf, e) {
  animTotalTime({
    duration: 1000,
    timing(t) {
      return easingFunctions[tf](t);
    },
    draw(progress) {
      const width = rec[e].offsetWidth
      rec[e].style.width = width + ( progress * 10)  + 'px';
    }
  });
}

create('linear', 0)
create('easeInOutQuad', 1)
create('easeInOutCubic', 2)
create('easeInOutQuart', 3)
create('easeInOutQuint', 4)
*/