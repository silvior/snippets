# Basic commands

### Add existing repo

```bash
git remote add origin git@bitbucket.org:name/repoName.git  
git push -u origin --all  
git push -u origin --tags 
``` 


### Quick workflow

```bash
git remote update
git status  # check status
git pull  # if needed
git add .
git add file.ext # specific file
git commit -m "mensagem"
git push origin branchName

git push -u origin branchName # push to remote and track, first time

git rm file.ext  # deleted file
git rm -r folderName  # delete folder and contents
git add -u # add and remove
```


### Branch

```bash
git checkout -b develop  # new branch 
git branch # see branches
git checkout branchName  # change branch 

git branch -u remoteName/branchName # existing branch to track a remote branch  

git branch -d branchName  # delete local branch
git push origin :branchName  #  delete origin branch 
```

### Unstage Changes
`git reset HEAD`


### Discard Changes
`git checkout -- .`


### Merge

```bash
git diff sourceBranch targetBranch  # differences

git merge branchName # in master branch

git merge branchName --squash --no-commit  # without commit log
git commit -m "stable develop release"

git checkout branchName exemple.txt folder/exemple/  # merge specific file and/or folder
```


### Log

```bash
git log --oneline --all --graph --decorate # graph with colors and ...
git log -10 --all --date-order # last 10
```


### Tag

```bash
git tag tagName 1b2e1d63ff # nº is the commit number
git push --tags # send tags
```


### Switching remote URLs SSH/HTTPS

```bash
git remote -v # check

git remote set-url origin https://accountName@bitbucket.org/accountName/repoName.git # SSH to HTTPS

git remote set-url origin git@bitbucket.org:accountname/reponame.git # HTTPS to SSH

git remote -v 
```

### Clear cache !!!
```bash
git rm -r --cached .
git add .
git commit -am 'git cache cleared'
git push
```

### Clone

```bash
git clone git@bitbucket.org:accountName/repoName.git

git clone https://accountName@bitbucket.org/accountName/repoName.git
```


Reference:  
 [Setting up a repository](https://www.atlassian.com/git/tutorials/setting-up-a-repository/) | 
 [git - the simple guide](http://rogerdudler.github.io/git-guide/) | 
 [Useful git commands](https://gist.github.com/abadongutierrez/9920732#feature-branch-workflow)










