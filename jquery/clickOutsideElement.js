// Reference:
// http://stackoverflow.com/a/3028037/2481821
// http://css-tricks.com/dangers-stopping-event-propagation/

$(document).click(function(event) {
    if(!$(event.target).closest('#menucontainer').length) {
        if($('#menucontainer').is(":visible")) {
            $('#menucontainer').hide();
        }
    }
});
