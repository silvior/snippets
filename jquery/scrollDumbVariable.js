// Reference:
// Can't remember where I saw this

var dumbVariable = (function() {
  var dumbVar   = false;

  var scrolVar  =  function() {

      var scrolTopDist = $(window).scrollTop();

      if ( scrolTopDist > 500 ) {
        if( !dumbVar ) {
		  dumbVar = true;
          console.log('Yes');
        }
      } else {
        if( dumbVar ) {
		  dumbVar = false;
          console.log('No');
        }
      }
  };

  $(window).on('scroll', scrolVar);

})();
