// Reference:
// http://stackoverflow.com/a/9363769/2481821

function myFunc( args ) {
  var defaults = {
    optOne: 'Some default',
    optTwo: 'Another default',
    optThree: 'Some other default'
  };
  args = $.extend({}, defaults, args);

  console.log(args.optOne, args.optTwo, args.optThree);
}

myFunc({
  optOne: "We'll override optOne and optThree...",
  optThree: "...leaving optTwo to use its default."
});
