## Windows 

**Symbolic link**  
 ``mklink linkName "C:\target\folder"`` symbolic link.  
 ``mklink /d linkName "C:\target\folder"`` directory symbolic link, don't ``del targetName`` the link.  
 Reference:
 [Windows File Junctions, Symbolic Links and Hard Links](http://devtidbits.com/2009/09/07/windows-file-junctions-symbolic-links-and-hard-links/)


**Create file**  
 ``call>file.ext`` or ``touch file.ext``  

**Directory/folder structure**  
 ``mkdir _builders\scss\partials _builders\includes public\css public\img public\js public\js\vendor``



